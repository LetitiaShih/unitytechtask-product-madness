﻿using System;
using UnityEngine;
using Server.API;
using Promises;
public class RequestData : MonoBehaviour
{
    GameplayApi gameplayApi;
    void Start()
    {
        gameplayApi = new Server.API.GameplayApi();
        gameplayApi.Initialise();
        gameplayApi.SetPlayerBalance((Int64)123456789);
    
        Promises.Promise<Int32> initialWin = gameplayApi.GetInitialWin();
        Promises.Promise<Int64> playerBalance = gameplayApi.GetPlayerBalance();
        Int64 a = new Int64();
        playerBalance.Resolve(a);
    
        //Debug.Log("playerBalance: "+ Convert.ToInt64(a));


    }

}

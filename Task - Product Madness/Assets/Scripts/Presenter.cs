﻿using UnityEngine;
using System.Collections;
public class Presenter : MonoBehaviour
{
    View _view;
    Model _model;
    int _gridWidth;
    int _gridHeight;
    void Awake()
    {
        _model = GameObject.FindObjectOfType<Model>();
        _view = GameObject.FindObjectOfType<View>();
        _model.readyToStart.AddListener(InitalizeGrid);
        _view.onElementMouseUp.AddListener(OnElementClicked);
    }
    void InitalizeGrid()
    {
//        Debug.Log("InitalizeGrid");
        _gridWidth = _model.gridWidth;
        _gridHeight = _model.gridHeight;
        _view.InitializeElementGrid(_gridWidth,_gridHeight);
        for(int j = 0; j < _gridWidth; j++)
        {
            for(int i = 0; i < _gridHeight; i++)
            {
                _view.CreateElements(j,i);
            }
        }
        
    }

    //trans view's x,y to j,i let model knows
    void OnElementClicked(int x, int y)//arraj i = y in world, j = x in world
    {
        //Debug.Log("_map[y,x]: " + y +"," + x + ":" + _model.map[y,x]);
        if(_model.map[y,x] == -1) 
        {
            //tell view to change the element texture to mine
            _view.ChangeElementToMine(x,y,_model.map[y,x]);
        }
        else
        {
            _view.ChangeElementToMine(x,y,_model.map[y,x]);
            _model.IsNoMineElement(y,x,_model.openedMap); //update openedMap and ask view to change the element texture to their count
            UpdateOpenedElements();
        }
    }
    void UpdateOpenedElements()
    {
        for(int j = 0; j < _gridWidth; j++)
        {
            for(int i = 0; i < _gridHeight; i++)
            {
                if(_model.openedMap [i,j])
                {
                    _view.ChangeElementToMine(j,i,_model.map[i,j]);
                }
            }
        }  
    }
    
}




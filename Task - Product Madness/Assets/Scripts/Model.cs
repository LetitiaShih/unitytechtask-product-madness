﻿
using UnityEngine;
using UnityEngine.Events;
using System.IO;
using System;

[System.Serializable]
public class ElementPosEvent : UnityEvent<int, int>
{
}

public class Model : MonoBehaviour
{

    public int gridWidth;
    public int gridHeight;
    int minesNum;
    public int[,] map;
    public bool[,] openedMap;
    public ElementPosEvent openCertainElement;
    public UnityEvent readyToStart;

    //生成map
    void Start()
    {
        string path = Application.streamingAssetsPath + "/map.json";
        string LoadData = File.ReadAllText(path);
        MapData mapData =  JsonUtility.FromJson<MapData>(LoadData);
        gridWidth = Convert.ToInt32(mapData.w);
        gridHeight = Convert.ToInt32(mapData.h);
        minesNum = Convert.ToInt32(mapData.minesNum);
        
        InitMap();
    }
    void InitMap()
    {
        map = new int[gridHeight,gridWidth];
        openedMap = new bool[gridHeight,gridWidth];
        SetElementCount();
        RandomPlaceMines();
        readyToStart.Invoke();
    }
    void RandomPlaceMines()
    {
        int minesCount = 0;

        while(minesCount < minesNum)
        {
            int i = UnityEngine.Random.Range(0,gridHeight);
            int j = UnityEngine.Random.Range(0,gridWidth);
            if(map[i,j] != -1)
            {
                map[i,j] = -1;
            Debug.Log("mine: " +i + "," + j + "map[i,j]: " + map[i,j]);
                minesCount++;
            }
        }
    }
    void SetElementCount() //the number on element
    {
        for(int i = 0; i < gridHeight; i++)
        {
            for(int j= 0; j < gridWidth; j++)
            {
                map[i,j] = adjacentMines(i,j);
                //Debug.Log(i + "," + j + "=" + map[i,j]);
            }
        }
    }
    int adjacentMines(int i, int j) 
    {
        int count = 0;
    
        if (mineAt(i,   j+1)) ++count; // right
        if (mineAt(i+1, j+1)) ++count; // bottom right
        if (mineAt(i+1, j  )) ++count; // bottom
        if (mineAt(i+1, j-1)) ++count; //bottom left
        if (mineAt(i,   j-1)) ++count; // left
        if (mineAt(i-1, j-1)) ++count; //top left
        if (mineAt(i-1, j  )) ++count; // top
        if (mineAt(i-1, j+1)) ++count; // top right
        return count; //設定element的texture
    }

    bool mineAt(int i, int j) //check mine
    {
        //check in range？check is mine?
        if (i >= 0 && j >= 0 && i < gridHeight && j < gridWidth) return map[i, j] == -1;
        return false;
    }

    public void IsNoMineElement(int i, int j, bool[,] opened) //check noMine position & uncover them
    {
        if (i >= 0 && j >= 0 && i < gridHeight && j < gridWidth) 
        {
            if (opened[i, j]) return;
    

            // 接近地雷了？那不必继续下去了 
            if (adjacentMines(i, j) > 0) return;
    
            opened[i, j] = true;
    
            IsNoMineElement(i-1, j, opened);
            IsNoMineElement(i+1, j, opened);
            IsNoMineElement(i, j-1, opened);
            IsNoMineElement(i, j+1, opened);
        }
    }
}
[Serializable]
public class MapData
{
    public string w;
    public string h;
    public string minesNum;

}

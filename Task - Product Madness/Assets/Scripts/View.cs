﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class View : MonoBehaviour
{
    public Element elementPrefab;
    // int _elementX;
    // int _elementY;
    Element[,] _elements = new Element[5,5];
    public ElementPosEvent onElementMouseUp;
    public void InitializeElementGrid(int x, int y)
    {
        _elements = new Element[x,y];
    }
    public void CreateElements(int x, int y) //position(x,y)
    {
        _elements[x,y] = (Element)Instantiate(elementPrefab,new Vector3(x, y, 0), Quaternion.identity);
        _elements[x,y].transform.SetParent(gameObject.transform);
        _elements[x,y].onMouseUpEvent.AddListener(OnElementMouseUp);
    }
    
    public void OnElementMouseUp(int x, int y)
    {
        onElementMouseUp.Invoke(x,y);
        Debug.Log("element: {" + x + "," + y + "} clicked");
    }
    public void ChangeElementToMine(int x, int y,int count)
    {
        _elements[x,y].loadTexture(count);
    }

}

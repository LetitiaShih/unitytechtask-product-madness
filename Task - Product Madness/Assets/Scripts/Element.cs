﻿using UnityEngine;
using UnityEngine.Events;


public class Element : MonoBehaviour {

    public bool mine;

    public Sprite[] emptyTextures;
    public Sprite mineTexture;
    public ElementPosEvent onMouseUpEvent;

    void Initalize () 
    {
        
    }

    public void loadTexture(int count) 
    {
        if (count == -1)
            GetComponent<SpriteRenderer>().sprite = mineTexture;
        else
            GetComponent<SpriteRenderer>().sprite = emptyTextures[count];
    }
    void OnMouseUp() 
    {  

        onMouseUpEvent.Invoke((int)transform.position.x, (int)transform.position.y);
    
    }
}
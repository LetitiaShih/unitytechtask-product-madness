﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine.UI;

[CustomEditor(typeof(PrefabToolEditor))]
public class PrefabToolEditor : EditorWindow
{
    string fixJson(string value)
    {
        value = "{\"Items\":" + value + "}";
        return value;
    }
    string fixImageAddr(string value)
    {
        value = value.Replace(".png", "");
        return value;
    }
    GameObject[] _prefabArr;
    ConfigedPrefab[] _configedPrefabsArr;

    int _configNum;
    string _folderAddr = "";
    string _searchResult;
    List<string> _foundPrefabs = new List<string>(); //name of Prefabs


    [MenuItem("Window/PrefabToolEditor")]

    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(PrefabToolEditor));
    }


    void OnGUI()
    {
        GUILayout.Label("Prefab Creation", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
        // GUILayout.FlexibleSpace();
        if (GUILayout.Button("LoadConfig!", GUILayout.MinWidth(80), GUILayout.ExpandWidth(false)))
        {
            LoadConfig();
        }
        if (GUILayout.Button("Create!", GUILayout.MinWidth(80), GUILayout.ExpandWidth(false)))
        {
            CreatePrefab();
        }
        if (GUILayout.Button("Cancel!", GUILayout.MinWidth(80), GUILayout.ExpandWidth(false)))
        {
            UndoCreate();
        }
        //GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.Label("Search Prefabs have text attached", EditorStyles.boldLabel);
        _folderAddr = EditorGUILayout.TextField("Folder Address: ", _folderAddr);
        if (GUILayout.Button("Search!", GUILayout.MinWidth(80), GUILayout.ExpandWidth(false)))
        {
            SearchText(_folderAddr);
        }
        GUILayout.Label(_searchResult, EditorStyles.boldLabel);

        GUILayout.BeginVertical();
        if (_foundPrefabs.Count > 0)
        {
            ListSearchedText();
        }
        GUILayout.EndHorizontal();

    }
    void LoadConfig()
    {
        string path = Application.streamingAssetsPath + "/data.json";
        string LoadData = File.ReadAllText(path);
        string jsonString = fixJson(LoadData);
        _configedPrefabsArr = JsonHelper.FromJson<ConfigedPrefab>(jsonString);
        _configNum = _configedPrefabsArr.Length;
    }
    void CreatePrefab()
    {
        GameObject worldCanvas = GameObject.FindGameObjectWithTag("Canvas");
        GameObject defaultPrefab = Resources.Load<GameObject>("CardPrefab");
        _prefabArr = new GameObject[_configNum];
        for (int i = 0; i < _configNum; i++)
        {
            _prefabArr[i] = Instantiate(defaultPrefab, Vector3.zero, Quaternion.identity);
            _prefabArr[i].transform.SetParent(worldCanvas.transform);
            ResetRectTransform(_prefabArr[i].GetComponent<RectTransform>());
            SetPrefabConfig(_prefabArr[i], i);
        }

    }
    void SetPrefabConfig(GameObject curPrefab, int index)
    {
        if (_configedPrefabsArr[index].text != null)
        {
            curPrefab.GetComponentInChildren<Text>().text = _configedPrefabsArr[index].text;
        }
        else
        {
            Debug.Log("No text data");
        }
        Color nowColor = new Color(255, 255, 255, 255);
        Image curImage = curPrefab.GetComponentInChildren<Image>();
        if (_configedPrefabsArr[index].color != null)
        {
            ColorUtility.TryParseHtmlString(_configedPrefabsArr[index].color, out nowColor);
            curImage.color = nowColor;
        }
        else
        {
            Debug.Log("No color data");
        }
        if (_configedPrefabsArr[index].image != null)
        {
            string imageAddr = _configedPrefabsArr[index].image;
            string imageNewAddr = fixImageAddr(imageAddr);
            curImage.sprite = Resources.Load<Sprite>(imageNewAddr);
            curImage.SetNativeSize();
        }
        else
        {
            Debug.Log("No image data");
        }

    }
    void SearchText(string folderAddr)
    {
        _foundPrefabs = new List<string>();
        string[] guids2;
        if (folderAddr.Length > 0)
        {
            guids2 = AssetDatabase.FindAssets("t:Prefab", new[] { folderAddr });
        }
        else
        {
            guids2 = AssetDatabase.FindAssets("t:Prefab");
        }

        foreach (string guid2 in guids2)
        {
            //Debug.Log(AssetDatabase.GUIDToAssetPath(guid2));
            GameObject contentsRoot = PrefabUtility.LoadPrefabContents(AssetDatabase.GUIDToAssetPath(guid2));
            if (contentsRoot.GetComponentInChildren<Text>() != null)
            {
                _foundPrefabs.Add(contentsRoot.name);
            }
        }
        if (_foundPrefabs.Count > 0)
        {
            _searchResult = "Prefabs Found: ";
        }
        else
        {
            _searchResult = "Prefabs NotFound! ";
        }
    }
    void ListSearchedText()
    {
        for (int i = 0; i < _foundPrefabs.Count; i++)
        {
            GUILayout.Label((string)_foundPrefabs[i], EditorStyles.label);
        }
    }
    void UndoCreate()
    {
        for (int i = 0; i < _configNum; i++)
        {
            SafeDestroy(_prefabArr[i]);
        }
    }
    void ResetRectTransform(RectTransform rectTransform)
    {
        rectTransform.localRotation = Quaternion.Euler(0, 0, 0);
        rectTransform.localPosition = Vector3.zero;
        rectTransform.localScale = Vector3.one;
    }
    void SafeDestroy(GameObject gameObject)
    {
        if (Application.isEditor)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

}

[Serializable]
public class ConfigedPrefab
{
    public string text;
    public string color;
    public string image;

}
public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}


